package metamapp

class HomePageController {

    def index = {

      // need to remove session variables  
      if( session.getAttribute("SimilarityMatrixData") != null ){
        session.removeAttribute("SimilarityMatrixData")
      }
      if( session.getAttribute("ProbabilityFactor") != null ){
        session.removeAttribute("ProbabilityFactor")
      }
      if( session.getAttribute("attributeList") != null ){
        session.removeAttribute("attributeList")
      }
      if( session.getAttribute("totalConditions") != null ){
        session.removeAttribute("totalConditions")
      }
      if( session.getAttribute("krpResult") != null ){
        session.removeAttribute("krpResult")
      }
      
    }
}
