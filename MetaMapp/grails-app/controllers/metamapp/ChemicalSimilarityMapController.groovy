package metamapp

import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile
import util.ChemSimilarityUtil
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import util.ValidateUtil
import org.apache.log4j.Logger
import util.FileUtility

class ChemicalSimilarityMapController {

    Logger gLogger = Logger.getLogger(ChemicalSimilarityMapController.class)
    
    def index = {

        // MultipartHttpServletRequest is used to read attached files
        if(request instanceof MultipartHttpServletRequest){
          MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;

          def ActionVal = mpr.getAttribute("ActionVal")

          if( ActionVal ==null || !ActionVal.toString().equalsIgnoreCase("Download")){
             if( params.txtProbabilityFactor != null ){
                  boolean isValidate = true
                  flash.message = ""
                  gLogger.info "============ Comming into submit mathod of ChemicalSimilarityMapController ================\n"
                  gLogger.info "parmas.txtProbabilityFactor :: ${params.txtProbabilityFactor}"
                  //gLogger.info "parmas.txtCSVData :: ${params.txtCSVData}"
                  gLogger.info "parmas.filePath :: ${params.filePath}"
                  gLogger.info "parmas.filePath.getClass :: ${params.filePath?.getClass()}"

                  CommonsMultipartFile f = (CommonsMultipartFile) mpr.getFile("filePath");

                  gLogger.info "Type : ${f.contentType}"

                  if(f.empty && (params.txtCSVData==null || params.txtCSVData.toString().trim().length() == 0 ) ){
                    isValidate = false
                    flash.message = "Error! Data is mandatory."
                    redirect(action:"index",params:params)
                  }else if( !f.empty && !f.contentType.toString().equalsIgnoreCase("text/plain") && !f.contentType.toString().equalsIgnoreCase("text/csv")
                          && !f.contentType.toString().equalsIgnoreCase("application/vnd.ms-excel") && !f.contentType.toString().equalsIgnoreCase("application/save-to-disk")){
                    isValidate = false
                    flash.message = "Error! Bad extension (contentType : ${f.contentType}), Upload only .csv or .txt extension file."
                    redirect(action:"index",params:params)
                  }else if( params.txtProbabilityFactor == null || !ValidateUtil.getInstance().isDouble(params.txtProbabilityFactor.toString()) ){
                    isValidate = false
                    flash.message = "Error! ProbabilityFactor is mandatory and should be a decimal number"
                    redirect(action:"index",params:params)
                  }else {
                     try{
                        if( Double.parseDouble(params.txtProbabilityFactor) < 0.01 || Double.parseDouble(params.txtProbabilityFactor) > 1.0 ){
                            isValidate = false
                            flash.message = "Error! Please enter Probability Factor between the range of 0.01 to 1.0"
                            redirect(action:"index",params:params)
                        }
                      }catch(NumberFormatException e){
                          isValidate = false 
                          flash.message = "Error! Enter valid Probability Factor."
                          redirect(action:"index",params:params)
                      }
                  }

                  if(isValidate){
                      String inputStr = "";

                      // read attached file and prepare string out of it
                      if(!f.empty)
                      {
                        Reader reader = new InputStreamReader(f.getInputStream());
                        inputStr = FileUtility.getInstance().readFileStreamAsString(reader);
                      }
                      else
                      {
                        inputStr = params.txtCSVData.toString();
                      }

                      session.setAttribute("SimilarityMatrixData",inputStr)
                      session.setAttribute("ProbabilityFactor",params.txtProbabilityFactor)
                      def fileName = f?.getOriginalFilename()

                      // if file name is not there, give it to a default name 
                      if(fileName == null || fileName.trim().length() == 0 ){
                        fileName = "default"
                      }

                      // remove extension of the file
                      if(fileName.toString().lastIndexOf(".") != -1){
                        fileName = fileName.toString().substring(0, fileName.toString().lastIndexOf("."))
                      }

                      // remove spaces from the filename if there any
                      fileName = fileName.replaceAll(" ","_");

                      return [params:params,"fileName":fileName]
                  }
             }
          }
        }else{

            // prepare result for export
            if( params.DownloadType != null )
            {
              gLogger.info '====================Dowmload Result==================='


              String appsType = params.DownloadType
              String networkApprochType = params.NetworkApprochType
              String resultStream = "";

              if(appsType!=null){

                String similarityMatrixData = null;

                if(session.getAttribute("SimilarityMatrixData")!=null){

                   similarityMatrixData = session.getAttribute("SimilarityMatrixData")

                }

                Double probabilityFactor = null;
                if(session.getAttribute("ProbabilityFactor")!=null){

                   probabilityFactor = Double.parseDouble(session.getAttribute("ProbabilityFactor").toString())

                }

                if( similarityMatrixData!=null && probabilityFactor!=null)
                {
                  resultStream = ChemSimilarityUtil.getInstance().processChemicalSimilarity(similarityMatrixData,probabilityFactor,networkApprochType,appsType)
                }else{
                  resultStream = "-No Result-"
                }

                def fileName = params.fileName
                if(fileName == null ){
                  fileName = "default"
                }

                response.contentType = ConfigurationHolder.config.grails.mime.types[params.format]
                response.setHeader("Content-disposition", "attachment; filename=${fileName}.${params.extension}")
                response.outputStream << resultStream
                response.outputStream.flush()
              }
            }
        }
    }

}
