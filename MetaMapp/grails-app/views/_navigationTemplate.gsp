<div id="leftPanel">
  <div class="PagePanel">
    <div class="panelTop">

    </div>
    <div class="panelBody">
      <h1><g:message code="Common.Applications"/></h1>
      <ul>
        <g:link controller="chemicalSimilarityMap">
        <li ${ this.controllerName!=null && this.controllerName.toString().trim().equalsIgnoreCase("chemicalSimilarityMap")?"class=\"controller-selected\"" : "class=\"controller\" onmouseout=\"this.className='controller'\" onmouseover=\"this.className='controller-selected'\""} >
            <g:message code="Apps.ChemicalSimilarityMap"/>
            <p class="description"><g:message code="Apps.ChemicalSimilarityMap.Description"/></p>
        </li>
        </g:link>

        <g:link controller="keggReactionPair">
        <li ${ this.controllerName!=null && this.controllerName.toString().trim().equalsIgnoreCase("keggReactionPair")?"class=\"controller-selected\"" : "class=\"controller\" onmouseout=\"this.className='controller'\" onmouseover=\"this.className='controller-selected'\""}>
            <g:message code="Apps.KEGGReactionPair"/>
            <p class="description"><g:message code="Apps.KEGGReactionPair.Description"/></p>
        </li>
        </g:link>

        <g:link controller="statisticsAttributes">
        <li ${ this.controllerName!=null && this.controllerName.toString().trim().equalsIgnoreCase("statisticsAttributes")?"class=\"controller-selected\"" : "class=\"controller\" onmouseout=\"this.className='controller'\" onmouseover=\"this.className='controller-selected'\""}>
            <g:message code="Apps.StatisticsAttributes"/>
            <p class="description"><g:message code="Apps.StatisticsAttributes.Description"/></p>
        </li>
        </g:link>
        <!--
        <li class="controller" onmouseout="this.className='controller'" onmouseover="this.className='controller-selected'"><g:link controller="calculateSimilarity"><g:message code="Apps.CalculateSimilarityMatrix"/></g:link>
          <p class="description"><g:message code="Apps.CalculateSimilarityMatrix.Description"/></p>
        </li>

        -->
      </ul>
    </div>
    <div class="panelBtm">
     </div>



    <div class="PagePanel">

      <div class="panelTop">

      </div>
      <div class="panelBody">
      <h1>Contact Us</h1>

      <div class="contactus">  
        <ul>
          <li><span>For Scientific Aspects:</span>
          <p>
            <ul class="child">
                <li><a href="mailto:dinkumar@ucdavis.edu">Dinesh K Barupal</a></li>
                <li><a href="mailto:ofiehn@ucdavis.edu">Oliver Fiehn</a></li>
            </ul>
          </p>
          </li>
          
          <li><span>For Technical Aspects:</span>
          <p>
            <ul class="child">
                <li ><a href="mailto:phaldiya@ucdavis.edu">Pradeep K Haldiya</a></li>
                <li ><a href="mailto:wohlgemuth@ucdavis.edu">Gert Wohlgemuth</a></li>
            </ul>
          </p>
          </li>
        </ul>
      </div>

     </div>




    </div>
    <div class="panelBtm">
     </div>

  </div>
</div>