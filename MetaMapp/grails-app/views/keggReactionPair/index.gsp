<html>
<head>
  <title><g:message code="Apps.KEGGReactionPair"/></title>
  <meta name="layout" content="main"/>
</head>
<body>

<div class="nav" >
<div class="menuButton">
   <a class="home" href="${createLink(uri: '/')}">Home</a>
</div>
</div>
<g:render template="/navigationTemplate"/>

<div id="pageBody">
  <div class="appsHeader"><h1><g:message code="Apps.KEGGReactionPair"/></h1></div>
  <g:form name="reactionPair" controller="keggReactionPair" enctype="multipart/form-data">

    <g:if test="${flash.message}">
      <div class="errors">
        ${flash.message}
      </div>
    </g:if>

    <table id='tabForm' align='center'>
      <tr height="30">
        <td class="lable"><g:message code="KEGGReactionPair.Ids"/></td>
        <td class="comma">:</td>
        <td class="field">
          <textarea name="txtIdPairs" class="form" title="Enter tab separated CIDs and KEGG ids pair">${params.txtIdPairs}</textarea>
        </td>
        <td class="info">
          <div class="info"><g:message code="KEGGReactionPair.Ids.Alert"/></div>
        </td>
      </tr>

      <tr height="30">
        <td class="lable"></td>
        <td class="comma"><g:message code="Common.OR"/></td>
        <td class="field">
          <input type="file" id="inputKeggCID" name="inputKeggCID" title="Upload only .csv or .txt extension file"/>
          <span><g:link controller="downloads" action="CID_KEGG_PAIR.txt" class="ex_file" title="click to download sample file" target="_blank">Sample File</g:link></span>
        </td>
        <td class="info">
          <div class="info"><g:message code="KEGGReactionPair.Ids.Or.Alert"/></div>
        </td>
      </tr>

      <tr height="30">
        <td class="lable"><g:message code="KEGGReactionPair.MatrixData"/></td>
        <td class="comma">:</td>
        <td class="field">
          <textarea name="txtCSVData" class="form" title="Upload the similarity matrix downloaded from PubChem">${params.txtCSVData}</textarea>
        </td>
        <td class="info">
          <div class="info"><g:message code="KEGGReactionPair.MatrixData.Alert"/></div>
        </td>
      </tr>

      <tr height="30">
        <td class="lable"></td>
        <td class="comma"><g:message code="Common.OR"/></td>
        <td class="field">
          <input type="file" id="filePath" name="filePath" title="Upload only .csv or .txt extension file"/>
          <span><g:link controller="downloads" action="Chemical_Similarity_Data_Matrix.csv" class="ex_file" title="click to download sample file" target="_blank">Sample File</g:link></span>
        </td>
        <td class="info">
          <div class="info"><g:message code="KEGGReactionPair.Or.Alert"/></div>
        </td>
      </tr>

      <tr height="30">
        <td class="lable"><g:message code="KEGGReactionPair.ProbabilityFactor"/></td>
        <td class="comma">:</td>
        <td class="field">
          <input type="text" id="txtProbabilityFactor" name="txtProbabilityFactor" title="Range between 0.01 to 1.0" value="${params?.txtProbabilityFactor != null ? params?.txtProbabilityFactor : 0.7}"/>
        </td>
        <td class="info">
          <div class="info"><g:message code="KEGGReactionPair.ProbabilityFactor.Alert"/></div>
        </td>
      </tr>

    </table>

    <br>
    <p align="center">
      <span class="buttons">
        <g:actionSubmit action="index" value="Submit" class="submit"/>
      </span>
    </p>
    <br>



    <% if ( fileName!=null ) { %>
    <table id='tabForm1' align='center'>
      <tr>
        <td colspan="5" align="left">
          <h1><g:message code="Common.Downloads"/></h1>
        </td>
      </tr>
      <tr>
        <td width="1%">&nbsp;</td>
        <td width="40%" colspan="5"><h1 class="subTitle">(A). Network-1(Lower Triangular Approch)</h1></td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:link params="[ActionVal:'Download',DownloadType:'SimilarityMap',NetworkApprochType:'LowerTriangularNetwork',format:'sif',fileName:fileName+'_SimilarityMapLTA',extension:'sif']" class="attachment"><g:message code="Attachment.SimilarityMap"/></g:link></td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:link params="[ActionVal:'Download',DownloadType:'EdgeAttribute',NetworkApprochType:'LowerTriangularNetwork',format:'text',fileName:fileName+'_EdgeAttributeLTA',extension:'txt']" class="attachment"><g:message code="Attachment.EdgeAttribute"/></g:link></td>
        <td width="1%">&nbsp;</td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:message code="Attachment.SimilarityMap.Description"/></td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:message code="Attachment.EdgeAttribute.Description"/></td>
        <td width="1%">&nbsp;</td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="40%" colspan="5"><h1 class="subTitle">(B). Network-2(Upper Triangular Approch)</h1></td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:link params="[ActionVal:'Download',DownloadType:'SimilarityMap',NetworkApprochType:'UpperTriangularNetwork',format:'sif',fileName:fileName+'_SimilarityMapUTA',extension:'sif']" class="attachment"><g:message code="Attachment.SimilarityMap"/></g:link></td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:link params="[ActionVal:'Download',DownloadType:'EdgeAttribute',NetworkApprochType:'UpperTriangularNetwork',format:'text',fileName:fileName+'_EdgeAttributeUTA',extension:'txt']" class="attachment"><g:message code="Attachment.EdgeAttribute"/></g:link></td>
        <td width="1%">&nbsp;</td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:message code="Attachment.SimilarityMap.Description"/></td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:message code="Attachment.EdgeAttribute.Description"/></td>
        <td width="1%">&nbsp;</td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="40%" colspan="5"><h1 class="subTitle">(C). KRP</h1></td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:link params="[ActionVal:'Download',DownloadType:'KEGGReactionPair',NetworkApprochType:'UpperTriangularNetwork',format:'sif',fileName:fileName+'_KEGGReactionPair',extension:'sif']" class="attachment"><g:message code="Attachment.KEGGReactionPair"/></g:link></td>
        <td width="1%">&nbsp;</td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:message code="Attachment.KEGGReactionPair.Description"/></td>
        <td width="1%">&nbsp;</td>
      </tr>

    </table>
    <% } %>
  </g:form>

</div>
</body>
</html>