<html>
    <head>
        <title>MetaMapp</title>
        <link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
        <link rel="shortcut icon" href="${resource(dir:'images',file:'m2.png')}" type="image/x-icon" />
        <g:layoutHead />
        <g:javascript library="application" />
    </head>
    <body>
        <div id="spinner" class="spinner" style="display:none;">
            <img src="${resource(dir:'images',file:'spinner.gif')}" alt="Spinner" />
        </div>
        <div id="grailsLogo" class="logo"><g:link controller="homePage">
          <img src="${resource(dir:'images',file:'MetaMapp.png')}" alt="Grails" border="0" />
        </g:link></div>
        <g:layoutBody />
    </body>
</html>