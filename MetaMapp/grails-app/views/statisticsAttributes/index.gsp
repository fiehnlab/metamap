<html>

<head>
  <title><g:message code="Apps.StatisticsAttributes"/></title>
  <meta name="layout" content="main"/>
</head>

<body>

<div class="nav" >
<div class="menuButton">
   <a class="home" href="${createLink(uri: '/')}">Home</a>
</div>
</div>
<g:render template="/navigationTemplate"/>

<div id="pageBody">

  <g:form name="statisticsAttributes" controller="statisticsAttributes" enctype="multipart/form-data">
    <div class="appsHeader"><h1><g:message code="Apps.StatisticsAttributes"/></h1></div>
    <g:if test="${flash.message}">
      <div class="errors">
        ${flash.message}
      </div>
    </g:if>

    <table id='tabForm' align='center'>
      <tr height="30">
        <td class="lable"><g:message code="StatisticsAttributes.NOCon"/></td>
        <td class="comma">:</td>
        <td class="field">
          <input type="text" id="txtNoCon" name="txtNoCon" title="Enter Number of condition" value="${params?.txtNoCon}"/>
        </td>
        <td class="info">
          <div class="info"><g:message code="StatisticsAttributes.NOCon.Alert"/></div>
        </td>
      </tr>

      <tr height="30">
        <td class="lable"><g:message code="StatisticsAttributes.Threshold"/></td>
        <td class="comma">:</td>
        <td class="field">
          <input type="text" id="txtThreshold" name="txtThreshold" title="Range between 0.01 to 1.0" value="${params?.txtThreshold}"/>
        </td>
        <td class="info">
          <div class="info"><g:message code="StatisticsAttributes.Threshold.Alert"/></div>
        </td>
      </tr>

      <tr height="30">
        <td class="lable"><g:message code="StatisticsAttributes.StatisticalData"/></td>
        <td class="comma">:</td>
        <td class="field">
          <input type="file" id="fileStatistics" name="fileStatistics" title="Upload only .csv extension file"/>
          <span><g:link controller="downloads" action="Statistical_Data.csv" class="ex_file" title="click to download sample file" target="_blank">Sample File</g:link></span>
        </td>
        <td class="info">
          <div class="info"><g:message code="StatisticsAttributes.StatisticalData.Alert"/></div>
        </td>
      </tr>
    </table>

    <br>
    <p align="center">
      <span class="buttons">
        <g:actionSubmit action="index" value="Submit" class="submit"/>
      </span>
    </p>
    <br>

    <%
      if (isNodeAttribute) {
    %>
    <table id='tabForm1' align='center'>
      <tr>
        <td colspan="5" align="left">
          <h1><g:message code="Common.Downloads"/></h1>
        </td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:link params="[ActionVal:'Download',DownloadType:'NodeAttribute',format:'text',fileName:fileName+'_NodeAttribute',extension:'txt']" class="attachment"><g:message code="Attachment.NodeAttribute"/></g:link></td>
        <td width="1%">&nbsp;</td>
        <td width="30%">&nbsp;</td>
        <td width="1%">&nbsp;</td>
      </tr>

      <tr>
        <td width="1%">&nbsp;</td>
        <td width="30%"><g:message code="Attachment.NodeAttribute.Description"/></td>
        <td width="1%">&nbsp;</td>
        <td width="30%">&nbsp;</td>
        <td width="1%">&nbsp;</td>
      </tr>
    </table>
    <% } %>
  </g:form>
</div>
</body>
</html>