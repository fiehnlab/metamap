<html>
<head>
  <title><g:message code="HomePage.Welcome"/></title>
  <meta name="layout" content="main"/>
</head>
<body>

<g:render template="/navigationTemplate"/>

<div id="welcomeBody">
  <h1><g:message code="HomePage.Welcome"/></h1>
  <div id="welcomeText">
    <g:message code="HomePage.Welcome.Text"/>
  </div>
</div>
</body>
</html>