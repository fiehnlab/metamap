<html>

<head>
  <title><g:message code="Apps.StatisticsAttributes"/></title>
  <meta name="layout" content="main"/>
  <g:javascript library="prototype"/>

</head>

<body>
<g:render template="/navigationTemplate"/>

<div id="pageBody">

  <g:if test="${flash.message}">
    <div class="errors">
      ${flash.message}
    </div>
  </g:if>

  <div class="description">
    This tool is used to generate chemical similarity matrixes.
  </div>


  <g:form name="calculateForm" action="calculateMatrix">

    <div class="left">Database</div>

    <div class="right">
      <g:select name="database"
              from="${databases}"
              value="${database}"

              noSelection="['':'-Choose your database-']"/>

    </div>
    <div class="left">
      BinBase Bin Id's

    </div>
    <div class="right">
      <g:textArea class="form" name="ids" rows="5" cols="20"/>

    </div>


    <div class="left">
    </div>

    <div class="right">
      <span class="buttons">
        <g:submitToRemote class="submit" name="startCalculation" value="Calculate Resultfile" url="[controller:'calculateSimilarity',action:'calculateMatrix']" update="downloadLink"/>

      </span>
    </div>

  </g:form>


  <div class="result" id="downloadLink">
    <!-- renders the download link -->
  </div>

</div>
</body>
</html>