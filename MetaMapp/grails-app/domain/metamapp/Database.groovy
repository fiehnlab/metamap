package metamapp

/**
 * defines our database
 */
class Database {

  /**
   * association
   */
  static hasMany = [
          bins: Bin
  ]

  static constraints = {
  }

  /**
   * contains all bins of this database
   */
  Set bins

  /**
   * name of the database
   */
  String name

  /**
   * description
   */
  String description
}
