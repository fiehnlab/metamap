package metamapp

/**
 * the bin
 */
class Bin {

  static belongsTo = [database: Database]


  static mappedBy = [similarities:'first']

  static hasMany = [
          similarities: BinSimilarity
  ]

  /**
   * associated database
   */
  Database database

  /**
   * bin id
   */
  int binId

  /**
   * the name of the bin
   */
  String name

  /**
   * the massspec of the bin
   */
  String massSpec


  String toString() {
    return "$binId - $name - $massSpec in database ${database.getName()}"
  }



  static constraints = {
    massSpec(maxSize: 15000, nullable: false, unique: false)
    name(maxSize: 5000, nullable: true)
  }
}
