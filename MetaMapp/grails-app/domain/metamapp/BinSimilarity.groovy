package metamapp

/**
 * stores the calculated bin similarity
 */
class BinSimilarity {

  static belongsTo = Bin

  static constraints = {
  }

  /**
   * first bin
   */
  Bin first

  /**
   * second
   */
  Bin second

  /**
   * the calculated similarity
   */
  double similarity


  public String toString() {
    return "${similarity}"
  }

}


