import util.KEGGPairUtil

class BootStrap {

  def init = { servletContext ->

    // Load the list of kegg-rpair list
    KEGGPairUtil.getInstance().loadKEGGPairList();  
  }


  def destroy = {
  }
} 