package util;

import org.apache.log4j.Logger;
import java.io.*;
import java.util.Date;
import java.util.Random;

/**
 * User: pradeep
 * Date: Dec 6, 2010
 * Time: 5:19:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileUtility {

    Logger logger = Logger.getLogger(FileUtility.class);
    private static FileUtility fileUtility = null;

    /**
     * To get the static instance of the Utility class
     * @return
     */
     public static FileUtility getInstance()
     {
         if( fileUtility == null )
         {
            fileUtility = new FileUtility();
         }

         return  fileUtility;
     }

    /**
     * to read the file into UTF-8 format and get byte[]
     * @param file
     * @return
     * @throws IOException
     */
    public byte[] getBytesFromFile(File file) throws IOException {

        String lStrFileData = "";
        try
        {
            String fileData = readFileAsString(file);
            lStrFileData = new String(fileData.getBytes(),"UTF-8");
        }catch(Exception e){
            e.printStackTrace();
        }
        return lStrFileData.getBytes();
    }

    /**
     * to read the file into UTF-8 format and get byte[]
     * @param fileUrl
     * @return
     * @throws IOException
     */
    public byte[] getBytesFromFile(String fileUrl) throws IOException {
        return getBytesFromFile(new File(fileUrl));
    }

    /**
     * to read the file into String
     * @param file
     * @return
     * @throws java.io.IOException
     */
    public static String readFileAsString(File file) throws java.io.IOException{
        /*byte[] buffer = new byte[(int) file.length()];
        BufferedInputStream f = null;
        try {
            f = new BufferedInputStream(new FileInputStream(file));
            f.read(buffer);
        } finally {
            if (f != null) try { f.close(); } catch (IOException ignored) { }
        }
        return new String(buffer);*/

        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(file));
            char[] buf = new char[1024];
            int numRead=0;
            while((numRead=reader.read(buf)) != -1){
                fileData.append(buf, 0, numRead);
            }
        } finally {
            if (reader != null) try { reader.close(); } catch (IOException ignored) { }
        }
        return fileData.toString();

    }

/**
     * to read the file into String
     * @param reader
     * @return
     * @throws java.io.IOException
     */
    public String readFileStreamAsString(Reader reader) throws java.io.IOException{
        StringBuffer fileData = new StringBuffer();
        //BufferedReader reader = null;
        try{
            //reader = new BufferedReader(new FileReader(file));
            char[] buf = new char[1024];
            int numRead=0;
            while((numRead=reader.read(buf)) != -1){
                fileData.append(buf, 0, numRead);
            }
        } finally {
            if (reader != null) try { reader.close(); } catch (IOException ignored) { }
        }
        return fileData.toString();

    }



    /**
     * to read the file into String
     * @param filePath
     * @return
     * @throws java.io.IOException
     */
    public static String readFileAsString(String filePath) throws java.io.IOException{
        return readFileAsString(new File(filePath));
    }


    public String getRendomFileName(){
        Random randomGenerator = new Random();
        String fileName = String.valueOf((new Date()).getTime())+"_"+randomGenerator.nextDouble();
        return fileName;
    }

}

