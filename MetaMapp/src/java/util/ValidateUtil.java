package util;

/**
 * User: pradeep
 * Date: Aug 16, 2010
 * Time: 3:44:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class ValidateUtil {

    private static ValidateUtil validUtil = null;

    public static ValidateUtil getInstance(){
        if( validUtil == null ){
            validUtil = new ValidateUtil();
        }
        return validUtil;
    }

    public boolean isInteger(String value){
        boolean validResult = false;
        String validString = "0123456789";

        for(int index=0;index<value.length();index++){
            if( validString.contains(""+value.charAt(index))){
                validResult = true;
            }else{
                validResult = false;
            }
        }
        return validResult;
    }

    public boolean isDouble(String value){
        boolean validResult = false;
        String validString = "0123456789.";

        for(int index=0;index<value.length();index++){
            if( validString.contains(""+value.charAt(index))){
                validResult = true;
            }else{
                validResult = false;
            }
        }

        //check for decimal point
        if(value.indexOf(".") != value.lastIndexOf(".")){
            validResult = false;
        }
        
        return validResult;
    }


}
