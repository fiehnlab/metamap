package util;

import junit.framework.TestCase;

import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 2/16/11
 * Time: 1:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileUtilityTests extends TestCase {

      public void testGetBytesFromFile(){
        byte[] result = null;
        try
        {
            File file = new File("test/Dataset/Chemical_Similarity_Data_Matrix.csv");
            result = FileUtility.getInstance().getBytesFromFile(file);
        }catch(Exception e){
            e.printStackTrace();
        }

        //System.out.println(result);
        //System.out.println(result.length);

        assertTrue(result!=null);
        assertTrue(result.length == 653);

      }

      public void testGetBytesFromFileByUrl(){
        byte[] result = null;
        try
        {
            String url = "test/Dataset/Chemical_Similarity_Data_Matrix.csv";
            result = FileUtility.getInstance().getBytesFromFile(url);
        }catch(Exception e){
            e.printStackTrace();
        }

        //System.out.println(result);
        //System.out.println(result.length);

        assertTrue(result!=null);
        assertTrue(result.length == 653);

      }

      public void testReadFileAsString(){
          String result = null;
          try
          {
              String url = "test/Dataset/Chemical_Similarity_Data_Matrix.csv";
              result = FileUtility.readFileAsString(url);
          }catch(Exception e){
            e.printStackTrace();
          }

          //System.out.println(result.length());

          assertTrue(result!=null);
          assertTrue(result.length() == 653);

      }
}
