package util;

import junit.framework.TestCase;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 2/16/11
 * Time: 2:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class KEGGPairUtilTests extends TestCase{

    public void testGetPairList(){

        List pairList = KEGGPairUtil.getInstance().getKEGGPairList();

//        System.out.println(pairList);
//        System.out.println(pairList.size());
//        System.out.println(pairList!=null);

        assertTrue(pairList!=null);
        assertTrue(pairList.size() == 12281);
    }

    public void testProcesskeggReactionPair(){

        String data = "PubChem\tKEGG\n" +
                "91\tC11457\n" +
                "107\tC05629\n" +
                "119\tC00334\n" +
                "190\tC00147\n" +
                "196\tC06104\n" +
                "203\tC00499\n" +
                "204\tC01551\n" +
                "229\tC00259\n" +
                "236\tC00152\n" +
                "236\tC00152\n" +
                "236\tC00152\n" +
                "239\tC00099\n" +
                "243\tC00180\n" +
                "309\tC00417\n" +
                "311\tC00158\n" +
                "338\tC00805\n" +
                "464\tC01586\n" +
                "469\tC00956\n" +
                "525\tC00149\n" +
                "588\tC00791\n" +
                "594\tC00097\n" +
                "604\tC00257\n" +
                "612\tC00186\n" +
                "700\tC00189\n" +
                "724\tC00197\n" +
                "743\tC00489\n" +
                "750\tC00037\n" +
                "753\tC00116\t";

        List result = KEGGPairUtil.getInstance().processkeggReactionPair(data);
        System.out.println(result);

        assertTrue(result!=null);
        assertTrue(result.size()==6);

    }
}


