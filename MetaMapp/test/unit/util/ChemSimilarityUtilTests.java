package util;

import junit.framework.TestCase;

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 2/16/11
 * Time: 1:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChemSimilarityUtilTests extends TestCase {

    public void testGetDelimiter(){
        String data = "1,2,3,4,5";

        char delimiter = ChemSimilarityUtil.getInstance().getDelimiter(data);

        assertTrue(delimiter == ',' );
    }

    public void testCreateMatrix(){
        String data = "0,2,3,4\n5,6,7,8\n,2,4,6,8\n23,65,67,8";

        double numMatrix[][] = ChemSimilarityUtil.getInstance().createMatrix(data);

        //System.out.println("Result :: "+numMatrix);
        //System.out.println("Result :: "+numMatrix[0][1]);

        assertTrue(numMatrix!=null);
        assertTrue(numMatrix[0][1] == 2.0);

    }

    public void testProcessData(){

        String data = "cid/cid,290,5789,6021,60961,790,1188,1004,1023,13130,787\n" +
                "290,1,0.644,0.474,0.455,0.243,0.245,0.1,0.122,0.154,0.044\n" +
                "5789,0.644,1,0.483,0.397,0.268,0.286,0.047,0.057,0.103,0.049\n" +
                "6021,0.474,0.483,1,0.783,0.688,0.658,0.029,0.036,0.057,0.03\n" +
                "60961,0.455,0.397,0.783,1,0.513,0.541,0.028,0.035,0.056,0.029\n" +
                "790,0.243,0.268,0.688,0.513,1,0.917,0.009,0.019,0.036,0.03\n" +
                "1188,0.245,0.286,0.658,0.541,0.917,1,0.017,0.026,0.042,0.027\n" +
                "1004,0.1,0.047,0.029,0.028,0.009,0.017,1,0.818,0.6,0.167\n" +
                "1023,0.122,0.057,0.036,0.035,0.019,0.026,0.818,1,0.625,0.143\n" +
                "13130,0.154,0.103,0.057,0.056,0.036,0.042,0.6,0.625,1,0.111\n" +
                "787,0.044,0.049,0.03,0.029,0.03,0.027,0.167,0.143,0.111,1";

        double numMatrix[][] = ChemSimilarityUtil.getInstance().createMatrix(data);

        double probFactor = 0.7;
        String networkApprochType = "UpperTriangularNetwork";
        String appsType = "EdgeAttribute";

        String result = ChemSimilarityUtil.getInstance().processData(numMatrix,probFactor,networkApprochType,appsType);

        //System.out.println("Result :: "+result);

        assertTrue( result.equals("6021 (tm) 60961\t0.783\n" +
                "790 (tm) 1188\t0.917\n" +
                "1004 (tm) 1023\t0.818"));

    }
}
